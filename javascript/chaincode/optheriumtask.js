/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class OptheriumTask extends Contract {

    async initLedger(ctx) {
        console.info('============= START : Initialize Ledger ===========');
        const incomes = [
            {
                key: 'key1',
                value: 'value1',

            },
            {              
                key: 'key2',
                value: 'value2',

            },
            {
                key: 'key3',
                value: 'value3',
            },
            {
                key: 'key4',
                value: 'value4',
            },
            {
                key: 'key5',
                value: 'value5',
            },
        ];

        for (let i = 0; i < incomes.length; i++) {
            incomes[i].docType = 'income';
            await ctx.stub.putState('INCOME' + i, Buffer.from(JSON.stringify(incomes[i])));
            console.info('Added <--> ', incomes[i]);
        }
        console.info('============= END : Initialize Ledger ===========');
    }

    async addIncome(ctx, key, value) {
        console.info('============= START : Add Income ===========');

        const income = {
            key,
            value,
            docType: 'income',
        };

        
        var payload = incpme;
        var payloadBuff = Buffer.from(JSON.stringify(payload));

        ctx.stub.setEvent("createCar", payloadBuff);

        await ctx.stub.putState(key, Buffer.from(JSON.stringify(income)));
        console.info('============= END : Add Income ===========');
    }

    async queryAllIncomes(ctx) {
        const startKey = 'INCOME0';
        const endKey = 'INCOME99';

        const iterator = await ctx.stub.getStateByRange(startKey, endKey);

        const allResults = [];
        while (true) {
            const res = await iterator.next();

            if (res.value && res.value.value.toString()) {
                console.log(res.value.value.toString('utf8'));

                const Key = res.value.key;
                let Record;
                try {
                    Record = JSON.parse(res.value.value.toString('utf8'));
                } catch (err) {
                    console.log(err);
                    Record = res.value.value.toString('utf8');
                }
                allResults.push({ Key, Record });
            }
            if (res.done) {
                console.log('end of data');
                await iterator.close();
                console.info(allResults);
                return JSON.stringify(allResults);
            }
        }
    }

}

module.exports = OptheriumTask;
