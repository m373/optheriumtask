/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { FileSystemWallet, Gateway } = require('fabric-network');
const fs = require('fs');
const path = require('path');
const client = require('fabric-ca-client');
const redis = require('redis');

const ccpPath = path.resolve(__dirname, 'connection.json');
const ccpJSON = fs.readFileSync(ccpPath, 'utf8');
const ccp = JSON.parse(ccpJSON);
const redisClient = redis.createClient();

async function main() {
        // Create a new file system based wallet for managing identities.
        const walletPath = path.join(process.cwd(), 'wallet');
        const wallet = new FileSystemWallet(walletPath);

        // Check to see if we've already enrolled the user.
        const userExists = await wallet.exists('user1');
        if (!userExists) {
            console.log('An identity for the user "user1" does not exist in the wallet');
            console.log('Run the registerUser.js application before retrying');
            return;
        }

        // Create a new gateway for connecting to our peer node.
        const gateway = new Gateway();
        await gateway.connect(ccp, { wallet, identity: 'user1', discovery: { enabled: false } });

        // Get the network (channel) our contract is deployed to.
        const network = await gateway.getNetwork('channeltwo');

        // Get the contract from the network.
        const contract = network.getContract('optheriumtask');


        try{
        var client = gateway.getClient();
        var channel = client.getChannel('channeltwo');

        var targets = ['peer0.org0.example.com', 'peer1.org0.example.com', 'peer0.org1.example.com',
        'peer1.org1.example.com'];


        let tx_id = client.newTransactionID();
        let request = {
            targets : targets,
            chaincodeId: 'optheriumtask',
            fcn: 'addIncome',
            args: ['key', 'value'],
            txId: tx_id
        };

        let results = await channel.sendTransactionProposal(request);

        var proposalResponses = results[0];
        var proposal = results[1];

        let isProposalGood = false;
        if (proposalResponses && proposalResponses[0].response && proposalResponses[0].response.status === 200) {
             isProposalGood = true;
             console.log('Transaction proposal was good');
        } else {
             console.error('Transaction proposal was bad');
        }
        if (isProposalGood) {
             console.log('Successfully sent Proposal and received ProposalResponse');

              var promises = [];
                let event_hubs = channel.getChannelEventHubsForOrg();
                event_hubs.forEach((eh) => {
                console.log(eh);
                let invokeEventPromise = new Promise((resolve, reject) => {
                let regid = null;
                let event_timeout = setTimeout(() => {
                    if(regid) {
                        let message = 'REQUEST_TIMEOUT:' + eh.getPeerAddr();
                        console.log(message);
                        eh.unregisterChaincodeEvent(regid);

                        block_reg = channel_event_hub.registerBlockEvent((block) => {
                            console.log('Successfully received the block event');
                            console.log(block)
                        }, (error)=> {
                            console.log('Failed to receive the block event ::'+error);
                        
                        },
                            {startBlock:23}
                        );
                        eh.disconnect();
                    }
                    reject(new Error('Timed out waiting for chaincode event'));
                }, 20000);
                regid = eh.registerChaincodeEvent('optheriumtask', 'addIncome',  (event, block_num, txnid, status)  => {
                console.log('Successfully got a chaincode event with transid:'+ txnid + ' with status:'+status);
                let event_payload = JSON.parse(event.payload.toString());
                console.log(event_payload);

                    clearTimeout(event_timeout);

                    redisClient.get(txnid, function (error, result) {
                        if (error) {
                            console.log("Error occured in redis");
                        }
                        if(result == null){
                          redisClient.set(JSON.stringify(event_payload), txnid, redis.print);
                        }
                    });
                    //Chaincode event listeners are meant to run continuously
                    //Therefore the default to automatically unregister is false
                    //So in this case we want to shutdown the event listener once
                    // we see the event with the correct payload
  resolve(event_payload);
                
                }, (err) => {
                clearTimeout(event_timeout);
                console.log(err);
                reject(err);
                }
                //no options specified
                //startBlock will default to latest
                //endBlock will default to MAX
                //unregister will default to false
                //disconnect will default to false
                );
                eh.connect(true);
            });
            promises.push(invokeEventPromise);
            });
    
            var requestMain = {
                txId: tx_id,
                proposalResponses: proposalResponses,
                proposal: proposal
            };
            var sendPromise = channel.sendTransaction(requestMain);
            promises.push(sendPromise);
            let result = await Promise.all(promises);
            console.log('------->>> R E S P O N S E : %j');
            console.log(result);
            let response = result.pop(); //  orderer results are last in the results
            if (response.status === 'SUCCESS') {
                console.log('Successfully sent transaction to the orderer.');
            } else {
                var error_message = ('Failed to order the transaction. Error code: %s' + response.status);
                console.log(error_message);
            }
    
            // now see what each of the event hubs reported
            for(let i in result) {
                let event_hub_result = result[i];
                let event_hub = event_hubs[i];
                console.log('Event results for event hub :%s' + event_hub.getPeerAddr());
                if(typeof event_hub_result === 'string') {
                    console.log(event_hub_result);
                    var rezultat = {event_payload : event_hub_result};
                    return rezultat;
                } else {
                    if(!error_message) error_message = event_hub_result.toString();
                    console.log(event_hub_result.toString());
                }
            }
        }else {
                error_message =('Failed to send Proposal and receive all good ProposalResponse');
                console.log(error_message);
        }
    }catch(error){
            console.log('Failed to add income: ' +  error.toString());
    }

        gateway.disconnect();

    

  
}

main();
